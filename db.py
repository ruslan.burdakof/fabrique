import sqlite3
from pytz import all_timezones_set
from datetime import datetime, timedelta, timezone
from utils import iscode, list_dict2tuple

TIMEZONE = all_timezones_set
HOME_DB = "db.sqlite3"

from pprint import pprint

def actv_mailings() -> [{}, {}, ...]:
    connect = sqlite3.connect(HOME_DB)
    cursor = connect.cursor()
    dtnow = datetime.now().isoformat()
    insert = f"""SELECT id, reach, created, stop, text
             FROM mailing_service_mailinglist
             WHERE created < '{dtnow}'
             AND stop > '{dtnow}'"""
    cursor.execute(insert)
    mailing_list = []
    for data in cursor.fetchall():
        if len(data) > 1:
            mailing_list.append({'id': data[0],
                                 'reach': data[1],
                                 'created': datetime.fromisoformat(data[2]),
                                 'stop': datetime.fromisoformat(data[3]),
                                 'text': data[4]})
        else:
            break
    connect.close()
    return mailing_list

def actv_mailings_column(column = 'id') -> (...):
    connect = sqlite3.connect(HOME_DB)
    cursor = connect.cursor()
    dtnow = datetime.now().isoformat()
    insert = f"""SELECT {column}
             FROM mailing_service_mailinglist
             WHERE created < '{dtnow}'
             AND stop > '{dtnow}'"""
    cursor.execute(insert)
    mailings = tuple((v[0] for v in cursor.fetchall()))
    connect.close()
    return mailings

def new_actv_mailings_id(old_mailings:tuple):
    present_mailing = actv_mailing_column(column='id')
    return tuple(set(old_mailings)-set(present_mailing))

def audience_mailings(mailings_id):
    connect = sqlite3.connect(HOME_DB)
    cursor = connect.cursor()
    dtnow = datetime.now().isoformat()
    insert = f"""SELECT reach
             FROM mailing_service_mailinglist
             WHERE id IN {mailings_id}"""
    cursor.execute(insert)
    audience = tuple((v[0] for v in cursor.fetchall()))
    connect.close()
    return audience 
    

def clients(mailing_id: str, reach = 'code') -> [{}, {}, ...]:
    connect = sqlite3.connect(HOME_DB)
    cursor = connect.cursor()
    insert = f"""SELECT id, phone, code, tag, timezone
             FROM mailing_service_client
             WHERE {'code' if iscode(reach) else 'tag'}='{reach}'"""
    cursor.execute(insert)
    clients_list = []
    for data in cursor.fetchall():
        if not data[4] in TIMEZONE:
            raise ValueError(f"Не известный часовой пояс {data[2]} клиента id {data[0]}")
        if len(data) > 1:
            clients_list.append({'id': data[0],
                                 'phone': data[1],
                                 'code': data[2],
                                 'tag': data[3],
                                 'timezone': data[4],
                                 })
    connect.close()
    return clients_list

def msgs_to_send(mailings_id:list) -> [{}, {}, ...]:
    msgs_list = []
    for mailing, reach in zip(mailings_id, audience_mailings(mailings_id)):
        for client in clients(mailing, reach):
            msgs_list.append({'status': 'draft',
                              'created': datetime.now().isoformat(),
                              'client_id': client['id'],
                              'mailing_id': mailing,
                              'request': 0,
                              'time_next_request': datetime.now().isoformat()})
    return msgs_list

def insert_msgs(mailings_id:str) -> None:
    if not mailings_id:
        return None
    connect = sqlite3.connect(HOME_DB)
    cursor = connect.cursor()
    insert = f"""INSERT INTO mailing_service_message
                (created,
                 client_id,
                 mailing_id,
                 status,
                 request,
                 time_next_request)
             VALUES (?,?,?,?,?,?)"""
    msgs = list_dict2tuple(msgs_to_send(mailings_id), ['created',
                                                       'client_id',
                                                       'mailing_id',
                                                       'status',
                                                       'request',
                                                       'time_next_request'])
    
    connect.executemany(insert, msgs)
    connect.commit()
    connect.close()


def actv_msgs():
    connect = sqlite3.connect(HOME_DB)
    cursor = connect.cursor()
    dtnow = datetime.now().isoformat()
    insert = f"""SELECT id,
                        created,
                        client_id,
                        mailing_id,
                        status,
                        request,
                        time_next_request
                 FROM mailing_service_message
                 WHERE (status = 'undelivered' and time_next_request < '{dtnow}'
                                               and '{dtnow}' < (SELECT stop
                                                                FROM mailing_service_mailinglist
                                                                WHERE id=mailing_id))
                    or (status = 'draft' and '{dtnow}' < (SELECT stop
                                                          FROM mailing_service_mailinglist
                                                          WHERE id=mailing_id))"""
    cursor.execute(insert)
    msgs = []
    for data in cursor.fetchall():
        if len(data) > 1:
            msgs.append({'id': data[0],
                         'created': datetime.fromisoformat(data[1]),
                         'client_id': data[2],
                         'mailing_id': data[3],
                         'status': data[4],
                         'request': data[5],
                         'time_next_request': datetime.fromisoformat(data[6])})
        else:
            break
    connect.close()
    return msgs

def update_msg(msg, param:dict):
    connect = sqlite3.connect(HOME_DB)
    cursor = connect.cursor()
    insert = f"""UPDATE mailing_service_message
              SET {', '.join([(f"{k}='{v}'" if v else f"{k}=NULL") for k,v in param.items()])}
              WHERE id = {msg['id']}"""
    cursor.execute(insert)
    connect.commit()
    connect.close()
def msg_post(msg_id):
    connect = sqlite3.connect(HOME_DB)
    cursor = connect.cursor()
    insert = f"""SELECT (SELECT phone FROM mailing_service_client WHERE id=client_id) AS phone,
                        (SELECT text FROM mailing_service_mailinglist WHERE id=mailing_id) AS text
                 FROM mailing_service_message
                 WHERE id = {msg_id}"""
    cursor.execute(insert)
    data = cursor.fetchone()
    connect.close()
    if not data:
        return None
    return {'id': msg_id,
            'phone': data[0],
            'text': data[1]}
