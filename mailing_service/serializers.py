from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from django.core.validators import RegexValidator

from .models import Client, MailingList, Message

class ClientSerializer(serializers.Serializer):
    '''Сериалайзер для клиентов'''
    id = serializers.IntegerField(required = False)
    phone = serializers.CharField(max_length = 11,
                                  validators = [UniqueValidator(Client.objects.all()),
                                                RegexValidator(regex = r"7\d{10}")])
    code = serializers.CharField(max_length = 3,
                                 required = False)
    tag = serializers.CharField(max_length = 50)
    timezone = serializers.CharField(max_length = 32)
    
    def create(self, validated_data):
        """
        Create and return a new `Client` instance, given the validated data.
        """
        validated_data.update({'code': validated_data['phone'][1:4]})
        return Client.objects.create(**validated_data)
    
    def update(self, instance, validated_data):
        instance.phone = validated_data.get('phone', instance.phone)
        instance.code = instance.phone[1:4]
        instance.tag = validated_data.get('tag', instance.tag)
        instance.timezone = validated_data.get('timezone', instance.timezone)
        return instance
   
class MailingSerializer(serializers.ModelSerializer):
    '''Сериалайзер рассылки'''
    class Meta:
        model = MailingList
        fields = '__all__'


