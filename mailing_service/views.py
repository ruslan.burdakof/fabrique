from django.shortcuts import render

from rest_framework.views import APIView
from rest_framework.response import Response

from rest_framework import status

from .models import Client, MailingList, Message
from .serializers import ClientSerializer, MailingSerializer

class ClientAPI(APIView):
    '''Клас представление для создания обработки данных клиента'''
    def get(self, request, format = None):
        '''Получение данных о клиенте по id'''
        client_id = request.data.get('id')
        if client_id:
            try:
                client = Client.objects.get(id = client_id)
                serializer = ClientSerializer(client)
                return Response(serializer.data)
            except Client.DoesNotExist:
                return Response(f"Not found Client[{client_id}]", status = status.HTTP_400_BAD_REQUEST)
            
        '''Получение данных о клиентах по списку id'''
        clients = (dict(request.data)).get('clients')
        if clients:
            clients = Client.objects.in_bulk(clients)
            serializer = ClientSerializer(clients.values(), many = True)
            return Response({'clients':serializer.data})
        return Response("Not found keys: 'id' or 'clients'", status = status.HTTP_400_BAD_REQUEST)                      

    def post(self, request, format = None):
        serializer = ClientSerializer(data = request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status = status.HTTP_201_CREATED)
        return Response(serializer.errors, status = status.HTTP_400_BAD_REQUEST)

    def put(self, request, format = None):
        client_id = request.data.get('id')
        if client_id:
            try:
                client = Client.objects.get(id = client_id)
                serializer = ClientSerializer(instance = client,
                                              data = request.data,
                                              partial = True)
                if serializer.is_valid():
                    update_client = serializer.update(client, request.data)
                    update_client.save()
                    return Response(serializer.data, status = status.HTTP_200_OK)
            except Client.DoesNotExist:
                return Response(f"Not found Client[{client_id}]", status = status.HTTP_400_BAD_REQUEST)
        return Response(serializer.errors, status = status.HTTP_400_BAD_REQUEST)
                        
    def delete(self, request):
        '''Удаление данных о клиенте по id'''
        client_id = request.data.get('id')
        if client:
            try:
                client = Client.objects.get(id = client_id)
                client.delete()
                return Response("Delete Client[{client_id}]", status = status.HTTP_200_OK)
            except Client.DoesNotExist:
                return Response("Not found Client[{client_id}]", status = status.HTTP_400_BAD_REQUEST)
        return Response("Not found key: id", status = status.HTTP_400_BAD_REQUEST)                      

class MailingListAPI(APIView):
    
    def post(self, request, format = None):
        serializer = MailingSerializer(data = request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status = status.HTTP_201_CREATED)
        return Response(serializer.errors, status = status.HTTP_400_BAD_REQUEST)

    def get(self, request, format = None):
        mailing_id = request.data.get('id')
        if mailing_id:
            try:
                mailing = MailingList.objects.get(id = mailing_id)
                serializer = MailingSerializer(mailing)
                return Response(serializer.data)
            except MailingList.DoesNotExist:
                return Response(f"Not found MailingList[{mailing_id}]", status = status.HTTP_400_BAD_REQUEST)
        return Response("Not found key: id", status = status.HTTP_400_BAD_REQUEST)

    def put(self, request, format = None):
        mailing_id = request.data.get('id')
        if mailing_id:
            try:
                mailing = MailingList.objects.get(id = mailing_id)
                serializer = MailingSerializer(instance = mailing,
                                               data = request.data,
                                               partial = True)
                if serializer.is_valid():
                    update_mailing = serializer.update(mailing, request.data)
                    update_mailing.save()
                    return Response(serializer.data, status = status.HTTP_200_OK)
            except MailingList.DoesNotExist:
                return Response(f"Not found MailingList[{mailing_id}]", status = status.HTTP_400_BAD_REQUEST)
        return Response("Not found key: id", status = status.HTTP_400_BAD_REQUEST)                      
    
    def delete(self, request):
        '''Удаление данных о рассылке по id'''
        mailing_id = request.data.get('id')
        if mailing_id:
            try:
                mailing = MailingList.objects.get(id = mailing_id)
                mailing.delete()
                return Response("Delete MailingList[{mailing_id}]", status = status.HTTP_200_OK)
            except MailingList.DoesNotExist:
                return Response(f"Not found MailingList[{mailing_id}]", status = status.HTTP_400_BAD_REQUEST)
        return Response("Not found key: id", status = status.HTTP_400_BAD_REQUEST)                      

class statisticAPI(APIView):
    def get(self, request, format = None):
        mailing_id = request.data.get('id')
        if mailing_id:
            try:
                sql = f"""SELECT 1 as id, count(*) as clients,
                                 sum(CASE status WHEN 'delivered'
                                     THEN 1 ELSE 0 END) as delivered
                          FROM mailing_service_message
                          WHERE mailing_id = {mailing_id}"""
                data = Message.objects.raw(sql)[0]
                return Response({'id': mailing_id, 
                                 'clients': data.clients,
                                 'delivered': data.delivered},
                                status = status.HTTP_200_OK)
            except Exception as e:
                print(e)
        return Response(f"Not found MailingList[{mailing_id}]", status = status.HTTP_400_BAD_REQUEST)
        

    
