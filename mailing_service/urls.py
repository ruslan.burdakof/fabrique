from django.urls import path

from . import views

urlpatterns = [
    path('api/clients/', views.ClientAPI.as_view()),
    path('api/mailing_list/', views.MailingListAPI.as_view()),
    path('api/statistic/', views.statisticAPI.as_view())
]
