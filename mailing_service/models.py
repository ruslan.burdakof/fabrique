from django.db import models
from django.core.validators import RegexValidator

models.UniqueConstraint(name='unique_messages',
                        fields=['client', 'mailing'],
                        deferrable=models.Deferrable.IMMEDIATE)


class Client(models.Model):
    '''Модель клиента'''
    phoneNumberRegex = RegexValidator(regex = r"7\d{10}")
    phone = models.CharField(validators = [phoneNumberRegex],
                             unique = True,
                             max_length = 11)
    
    code = models.CharField(max_length = 3)
    
    tag = models.CharField(max_length = 50)
    
    timezone = models.CharField(max_length = 32,
                                default = "UTC")    
    
class MailingList(models.Model):
    '''Модель рассылки'''
    reach = models.CharField(max_length = 50) 
    created = models.DateTimeField()
    stop = models.DateTimeField()
    text = models.CharField(max_length = 500)

class Message(models.Model):
    '''Модель сообщения'''
    client = models.ForeignKey(Client, on_delete = models.CASCADE)
    mailing = models.ForeignKey(MailingList, on_delete = models.CASCADE)
    status = models.CharField(max_length = 32,
                              null = True)
    created = models.DateTimeField()
    request = models.PositiveSmallIntegerField(null = True)
    time_next_request = models.DateTimeField(null = True)
    class Meta:
        constraints = [models.UniqueConstraint(fields=["client", "mailing"],
                                               name="uniq_message")]
