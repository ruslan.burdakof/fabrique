from datetime import datetime, timedelta, timezone
import time
import db

import asyncio
from aiohttp import ClientSession

with open('token','r') as file:
    TOKEN = file.readline()

HEADER = {'accept': 'application/json',
          'Authorization': f"Bearer {TOKEN}",
          'Content-Type': 'application/json'}


##from random import randint as rint
##async def sent_msg(msg):
##    await asyncio.sleep(0.02)
##    return rint(0, 10)<2

##async def cycle_sent_msgs(msgs) :
##    tasks_ = []
##    ioloop = asyncio.new_event_loop()
##    asyncio.set_event_loop(ioloop)
##    for msg in msgs:
##        tasks_.append(asyncio.create_task(sent_msg(msg)))
##    results = await asyncio.gather(*tasks_)
##    ioloop.close()
##    print(results)
##    return results

async def sent_msg(msg_id):
    json = db.msg_post(msg_id)
    if not json:
        return False
    url = f'https://probe.fbrq.cloud/v1/send/{msg_id}'
    async with ClientSession(headers = HEADER) as session:
        async with session.post(url=url, json=json) as response:
            data = await response.json()
            if response.status != 200:
                return False
    return True
        
async def cycle_sent_msgs(msg_ids):
    tasks = []
    for msg_id in msg_ids:
        tasks.append(asyncio.create_task(sent_msg(msg_id,)))
    results = await asyncio.gather(*tasks)
    return results    


def main(repeated_req=[10,20,60,120], task = 5):
    mailings_id = db.actv_mailings_column(column='id')
    db.insert_msgs(mailings_id)
    while True:
        time.sleep(0.2)
        tasks_ = []
        msgs_ = []
        
        msgs = db.actv_msgs()
        if not msgs:
            print("Not active msg")
            continue

        if len(msgs) < task:
            msgs_ = msgs
            msgs = []
        else:
            msgs_ = msgs[:task]

        for msg in msgs_:
            db.update_msg(msg, {'status': 'query'})
        msgs_id = [msg['id'] for msg in msgs_]
        print(msgs_id)

        results = asyncio.run(cycle_sent_msgs(msgs_id))

        print(results)
        for inx, result in enumerate(results):
            msg = msgs_[inx]
            if result:
                param = {'status': 'delivered'}
            else:
                t = msg['time_next_request']
                req = msg['request']
                if req == len(repeated_req):
                    t_next = None
                    req_next = req
                else:
                    t_next = t + timedelta(seconds = repeated_req[req])
                    t_next = t_next.isoformat()
                    req_next = req + 1
                param = {'status': 'undelivered',
                         'time_next_request': t_next,
                         'request': req_next}
            db.update_msg(msg, param)

import db_test as db_test
filling.mailing_cleare()
filling.clients_cleare()
filling.msgs_cleare()
filling.mailing_filing(delta = (30, 40))
filling.clients_filing(length = 3)

if __name__ =='__main__':
    main()
