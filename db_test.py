from datetime import datetime, timedelta
from random import randint as rint

from pprint import pprint
import sqlite3
from utils import iscode

def _phone(code:str = None):
    if iscode(code):
        return f"7{code}{rint(1000000,9999999)}"
    else:
        raise ValueError(f"Не правильный код оператора: {code}")
    return f"{rint(79000000000, 79999999999)}"

def _client(code, tag, timezone = "UTC"):
    return {"tag": tag, "timezone": timezone, "phone": _phone(code), "code": code}

def _mailing(reach: str, created: datetime, stop: datetime, text:str):
    return {"reach": reach, "created": created.isoformat(),
            "stop": stop.isoformat(), "text": text}

def clients_filing(codes = ['912', '999', '967', '900', '930'],
                   tags = ['test1', 'test1', 'test2', 'test3', 'test1'],
                   tzones = 5*["UTC"],
                   length =50):
    clients = []
    for cod, tag in zip(codes,tags):
        for _ in range(length):
            clients.append(tuple(_client(cod, tag).values()))

    connect = sqlite3.connect('db.sqlite3')
    cursor  = connect.cursor()
    
    insert = """INSERT INTO mailing_service_client
    (tag, timezone, phone, code)
    VALUES (?,?,?,?)"""
    cursor.executemany(insert, clients)
    connect.commit()
    connect.close()

def mailing_filing(codes = ['912', '999', '967', '900', '930'],
                   tags = ['test1', 'test1', 'test2', 'test3', 'test1'],
                   delta = (20, 8*20)):
    mailing = []
    for reach in (*tuple(codes), *tags):
        created = datetime.now() #+ timedelta(seconds = rint(*delta))
        stop = created + timedelta(seconds = rint(*delta))
        mailing.append(tuple(_mailing(reach,
                                      created,
                                      stop,
                                      f"Audit: {reach}").values()))

    connect = sqlite3.connect('db.sqlite3')
    cursor  = connect.cursor()
    insert = """INSERT INTO mailing_service_mailinglist
    (reach, created, stop, text)
    VALUES (?,?,?,?)"""
    cursor.executemany(insert, mailing)
    connect.commit()
    connect.close()
    
def clients_cleare():
    connect = sqlite3.connect('db.sqlite3')
    cursor  = connect.cursor()
    cursor.executescript("""DELETE FROM mailing_service_client;
                         UPDATE SQLITE_SEQUENCE SET seq = 0
                         WHERE name = 'mailing_service_client'""")
    
    connect.commit()
    connect.close()

def mailing_cleare():
    connect = sqlite3.connect('db.sqlite3')
    cursor  = connect.cursor()
    cursor.executescript("""DELETE FROM mailing_service_mailinglist;
                         UPDATE SQLITE_SEQUENCE SET seq = 0
                         WHERE name = 'mailing_service_mailinglist'""")
    connect.commit()
    connect.close()

def msgs_cleare():
    connect = sqlite3.connect('db.sqlite3')
    cursor  = connect.cursor()
    cursor.executescript("""DELETE FROM mailing_service_message;
                         UPDATE SQLITE_SEQUENCE SET seq = 0
                         WHERE name = 'mailing_service_message'""")
    connect.commit()
    connect.close()

mailing_cleare()
clients_cleare()
mailing_filing(delta = (10, 40))
clients_filing()


