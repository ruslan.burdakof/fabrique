import requests
import random
from datetime import datetime, timedelta
import json
import random

def _phone():
    return str(random.randint(79000000000, 79999999999))

url = 'http://localhost:8000/api/mailing_list/'
created = datetime.now()
stop = created + timedelta(minutes = random.randint(60, 8*60))

print("Create mailing")
req_create = {"reach": "test",
              "created": created.isoformat(),
              "stop": stop.isoformat(),
              "text": "umba-yumba"}
response = requests.post(url, data = req_create)
last_id = response.json()['id']
print(f'post:\n{req_create}\nres:\n{response.json()}\n')

print("Get mailing")
req_get = {"id": "1"}
response = requests.get(url, data = req_get)
print(f'get:\n{req_get}\nres:\n{response.json()}\n')

print("Update mailing")
req_put = {"id": "1",
           "reach": "test_post",
           "created": created.isoformat(),
           "stop": stop.isoformat(),
           "text": "umba-yumba"}
response = requests.put(url, data = req_put)
print(f'put:\n{req_put}\nres:\n{response.json()}')
req_get = {"id": "1"}
response = requests.get(url, data = req_get)
print(f'get:\n{req_get}\nres:\n{response.json()}\n')

print("Delete mailing")
req_delete = {"id": last_id}
response = requests.delete(url, data = req_delete)
print(f'delete:\n{req_put}\nres:\n{response.json()}')
req_get = {"id": last_id}
response = requests.get(url, data = req_get)
print(f'get:\n{req_get}\nres:\n{response.json()}\n')

url = r'http://localhost:8000/api/clients/'

print("Create client")
req = {"phone": _phone(), "tag": "test_post", "timezone": "UTC"}
response = requests.post(url, data = req)
print(f'post:\n{req_create}\nres:\n{response.json()}\n')

print("Get and update client")
req_get = {"id": "1"}
client_last = requests.get(url, data = req_get)
req_update = {"id": "1", "phone": _phone(), "tag": "test_post", "timezone": "UTC"}
response = requests.put(url, data = req_update)
client_now = requests.get(url, data = req_get)
print(f'client_last\n{client_last.json()}\nclient_now\n{client_now.json()}\n')

print("Create and delete client")
req_get = {"id": "12"}
req = {"phone": _phone(), "tag": "test_post", "timezone": "UTC"}
response = requests.post(url, data = req)
client_id = response.json()['id']
response = requests.delete(url, data = {"id": client_id})
print(response.json())


