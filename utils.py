def iscode(code):
    if code.isdigit() and len(code) == 3:
            return True
    return False

def list_dict2tuple(list_dict, keys:list) -> ((), (), ...):
    for dct in list_dict:
        yield tuple(dct[key] for key in keys)

